import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TableModule } from 'primeng/table';


import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SelectComponent } from './select/select.component';
import { ValidateComponent } from './validate/validate.component';
import { HeaderComponent } from './header/header.component';
import { SelectedComponent } from './selected/selected.component';
import { EncodeSpacesPipe } from './encode-spaces.pipe';
import { FooterComponent } from './footer/footer.component';
import { SafeHtmlPipe } from './safe-html.pipe';
import { SendToComponent } from './send-to/send-to.component';
import { SendHistoryComponent } from './send-history/send-history.component';
import { CartComponent } from './cart/cart.component';
import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';
import { environment } from '../environments/environment';
import { ExcelUploadComponent } from './excel-upload/excel-upload.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectComponent,
    ValidateComponent,
    HeaderComponent,
    SelectedComponent,
    EncodeSpacesPipe,
    FooterComponent,
    SafeHtmlPipe,
    SendToComponent,
    SendHistoryComponent,
    CartComponent,
    ExcelUploadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    ReactiveFormsModule,
    TableModule
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: environment.recaptcha.siteKey,
      } as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
