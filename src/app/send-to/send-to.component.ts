import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-send-to',
  templateUrl: './send-to.component.html',
  styleUrls: ['./send-to.component.scss']
})
export class SendToComponent implements OnInit {
  // quantity = this._http.quantity;
  enableExcel = false;
  errorText = 'אירעה שגיאה, אנא פנו לתמיכה';
  fileErrorText = 'שגיאה בקריאת הקובץ'
  item: any;
  isDemo: boolean;
  orderId: any;
  fileUploaded = false;
  sendForm: FormGroup;
  fileForm: FormGroup;
  sendPhone: boolean = false;
  sendMail: boolean = false;
  isGiftSite: boolean;
  nCLoadSum: number;
  noObligo: boolean = false;
  orderType: any;
  isError = false;
  isLoading: boolean = false;
  isTBWallet = false;
  biggerThan5: boolean = false;
  purposeSendingOpts: string[];
  workersData: any;
  showPurposeSending: any;
  IsObligo: any;
  isEmployeeIncentive = false;
  recaptchaToken: string;
  excelLoadSum = '';
  excelGreeting: string;
  numOfCUstsInFile = 0;
  isObligoDemoWithPassword = false;
  constructor(private _http: HttpService, private router: Router) {}

  ngOnInit() {
    this.orderId = this._http.ids.OrderId;
    this.item = this._http.choice.value;
    this.isGiftSite = (this.item && this.item.id == this._http.GIFTSITE_ITEM_ID)||this.orderId == 'KMZQ2OsZdXv5mv5cKp6B6Q2';
    this.orderType = this._http.order.value.Type;
    this.IsObligo = this._http.order.value.Type == 2;
    this.isObligoDemoWithPassword = this.orderId == 'd3oGH0BwU8wsQIkSQnVnrA2';
    this.enableExcel = this.IsObligo;
    // this.enableExcel = this.orderId == 'oW_BA0uV5MHWP1jfpCYa1w2';
    this.isTBWallet = (this.orderId == 'hK4pC5WRViKpJJ02jyd2CQ2' || this.orderId == '50WisZjEmfxWv1WR6BW2bQ2' || this.orderId == 'b1IZTwxNvoTty8t1HS0JVQ2' || this.orderId == 'd3oGH0BwU8wsQIkSQnVnrA2');
    this.showPurposeSending = (this.orderId == 'yDfkx4yAeH6yRworg-ZfrQ2' || this.orderId == 'YJiZLYHB_N7JQJglsqipUw2');
    this.purposeSendingOpts = ['תמריץ לעובד','Happy hour'];
    if (!this._http.checkValidation()) return;
    this.sendForm = new FormGroup({
      'loadSum': new FormControl(),
      'quantity': new FormControl(null, Validators.required),
      'recipient': new FormControl(),
      'from': new FormControl(),
      'greeting': new FormControl(),
      'phone': new FormControl('', Validators.pattern('^05[0-9]{8}$')),
      'email': new FormControl('', Validators.email),
      'purposeSending': new FormControl(),
      'workerId': new FormControl()
    }, this.checkSend);
    this.isDemo = this._http.order.value.isdemo;
    this.setFieldRequired();
  }

  checkSend(fg: FormGroup) {
    return fg.get('phone').value == '' && fg.get('email').value == '' ? { 'noSend': true } : null;
  }


  onSendSubmit() {
    
    if (this.showPurposeSending && !this.fileUploaded)
      if (!this.isEmployeeIncentive || this.isEmployeeIncentive && !this.sendForm.get('workerId').value) {
        this.isLoading = false;
        this.isError = true;
        this.errorText = 'חובה לבחור בשדה מטרת השליחה ולהזין את מספר העובד'
        return;
      }

    if (this.quantity.value <= 20 && this.quantity.value != 0) {
      if (this.item.Price * this.quantity.value <= this._http.employee.value.Obligo) {
        let choice: any;
        this.isLoading = true;
        this.isError = false;
        if (this._http.order.value.Type === 2)
        choice = {
          OrderId: this._http.ids.OrderId,
          ValidationField: this._http.ids.ValidationField,
          ItemId: this.item.id,
          MngChoices: [{Name: this.sendForm.get('recipient').value, //in obligo orders send an array
          FromWho: this.sendForm.get('from').value,
          MailMsg: this.sendForm.get('greeting').value}],
          Token: this._http.employee.value.token
        };
        // choice = {
        //   OrderId: this._http.ids.OrderId,
        //   ValidationField: this._http.ids.ValidationField,
        //   ItemId: this.item.id,
        //   Name: this.sendForm.get('recipient').value, //in obligo orders send an array
        //   FromWho: this.sendForm.get('from').value,
        //   MailMsg: this.sendForm.get('greeting').value,
        //   Token: this._http.employee.value.token
        // };
        else choice = {
          OrderId: this._http.ids.OrderId,
          ValidationField: this._http.ids.ValidationField,
          ItemId: this.item.id,
          Name: this.sendForm.get('recipient').value,
          FromWho: this.sendForm.get('from').value,
          MailMsg: this.sendForm.get('greeting').value,
        };
        if(this.showPurposeSending) choice.MngChoices[0].WorkerIdentifier = this.isEmployeeIncentive ? this.sendForm.get('workerId').value : this.sendForm.get('purposeSending').value;
        // if(this.showPurposeSending) choice.WorkerIdentifier = this.isEmployeeIncentive ? this.sendForm.get('workerId').value : this.sendForm.get('purposeSending').value;
        if (this.item.IsGiftCard && this._http.order.value.Type === 2) choice.LoadSum = this.sendForm.get('loadSum').value;
        else if (this.item.IsGiftCard) choice.LoadSum = this.item.LoadSum;
        if (this._http.order.value.Type === 2) {
          if (this.sendPhone) {
            let phone = this.sendForm.get('phone');
            if (phone.value && phone.valid) choice.MngChoices[0].Phone = phone.value;
            // if (phone.value && phone.valid) choice.Phone = phone.value;
          }
          if (this.sendMail) {
            let email = this.sendForm.get('email');
            if (email.value && email.valid) choice.MngChoices[0].Email = email.value;
            // if (email.value && email.valid) choice.Email = email.value;
          }
        }
        else {
          if (this.sendPhone) {
          let phone = this.sendForm.get('phone');
          if (phone.value && phone.valid) choice.Phone = phone.value;
        }
        if (this.sendMail) {
          let email = this.sendForm.get('email');
          if (email.value && email.valid) choice.Email = email.value;
        }
      }

       if (this.fileUploaded == true && this.workersData){// multiple sending
        if (this.item.IsGiftCard) {
          if (this.excelLoadSum == '') {
            this.isLoading = false;
            this.isError = true;
            this.fileErrorText = 'חובה למלא סכום טעינה';
            return;
          }
        }
        let sendChoice = [];
        choice = {
          OrderId: this._http.ids.OrderId,
          ValidationField: this._http.ids.ValidationField,
          ItemId: this.item.id,
          MngChoices: sendChoice,
          LoadSum: this.excelLoadSum,
          Token: this._http.employee.value.token
        };
        for (let i = 0; i < this.workersData.length; i++) { 
          sendChoice.push({
           Name: this.workersData[i].name,
           Phone: this.workersData[i].phone,
           Email: this.workersData[i].mail,
           WorkerIdentifier: this.workersData[i].workerNum,
           MailMsg: this.excelGreeting
         })
           
           choice.MngChoices = sendChoice;
         }
       }
        
        this._http.doChoiceMultiple(<JSON>choice).subscribe({
            next: (res: any) => {
              if (res.errdesc == 'NOT_ALLOWD_USER'){ 
                this.isError = true; this.isLoading = false;}
              if ( res.errdesc == 'DISCONNETCED') {
                this.isError = true; this.isLoading = false;
                this.errorText = 'עבר זמן מאז ההתחברות האחרונה שלך. עליך להתחבר מחדש.'
                this.fileErrorText = 'עבר זמן מאז ההתחברות האחרונה שלך. עליך להתחבר מחדש.'
              }
              if (res.err){
                this.isError = true; this.isLoading = false;
                if (res.err == -31){
                  this.errorText = 'אחד מהתווים שהזנת באחד מהשדות לא חוקיים. אנא מחק/י תוים מיוחדים'
                  this.fileErrorText = 'אחד מהתווים שהזנת באחד מהשדות לא חוקיים. אנא מחק/י תוים מיוחדים'
                }
                if ( res.err == -4){
                  this.errorText = 'שגיאת אבטחה. כתובת הIP אינה זהה לכתובת ההתחברות.'
                  this.fileErrorText = 'שגיאת אבטחה. כתובת הIP אינה זהה לכתובת ההתחברות.'
                }
                if ( res.err == -2 ){
                  this.errorText = 'עבר זמן מאז ההתחברות האחרונה שלך. עליך להתחבר מחדש.'
                  this.fileErrorText = 'עבר זמן מאז ההתחברות האחרונה שלך. עליך להתחבר מחדש.'
                }
                if (res.err == -3)
                  this.errorText = 'אירעה שגיאה, אנא פנה לתמיכה'
            }
          },
            complete: () => {
              if (!this.isError){
                this.isLoading = true;
                this.reValidate();
              }
            },
            error: () => { debugger; this.isError = true; this.isLoading = false; },
          })
        }

      
      else {
        this.noObligo = true;
      }
    }
    else{
      this.biggerThan5 = true;
    }
  }

  cenceLoadFile() {
    this.fileUploaded = false;
    this.workersData = '';
  }

  onPurposeSendingChanged() {
    this.isEmployeeIncentive = this.sendForm.get('purposeSending').value === this.purposeSendingOpts[0];
  }

  reValidate() {
    let employee: any;
    if (this._http.order.value.Type === 2)
      employee = {
        ValidationField: this._http.ids.ValidationField,
        OrderId: this._http.ids.OrderId,
        PackageId: this._http.linkedTbid.value,
        Token: this._http.employee.value.token,       
      }
      else employee = <JSON>this._http.ids;
    this._http.validateEmployee(employee, this._http.order.value.MultipleChoice == 1).subscribe({
      next: (data: any) => { if (data.response) {
        employee = data.response[0];
        employee.token = data.Token;
        let tempItems = this._http.employee.value.items;
        this._http.employee.next(employee) 
        this._http.employee.value.items = tempItems;
      } }, complete: () => {
        setTimeout(()=>{
          this._http.employee.value.history.reverse();
          this.router.navigate(['/send-history'], { queryParamsHandling: 'preserve' })
        },5000)


      }
    });
    
  }

  setFieldRequired() {
    let validators = this.isEmployeeIncentive && this.quantity.value < 2 ? [Validators.required] : [];
    this.sendForm.get('workerId').setValidators(validators);
    this.sendForm.get('workerId').updateValueAndValidity(); // Update the field's validity
    validators = !(this.isEmployeeIncentive && this.quantity.value >1) ? [Validators.required] : [];
    this.sendForm.get('recipient').setValidators(validators);
    this.sendForm.get('recipient').updateValueAndValidity();
    // this.sendForm.get('recipient').setValidators(validators);
    // this.sendForm.get('recipient').updateValueAndValidity();
    
  }

  get quantity() { return this.sendForm.get('quantity'); }
  get recipient() { return this.sendForm.get('recipient'); }
  get from() { return this.sendForm.get('from'); }
  get greeting() { return this.sendForm.get('greeting'); }
  get workerId() { return this.sendForm.get('workerId'); }
  get recaptcha() { return this.sendForm.get('recaptcha'); }

  uploadWorkersFILE(workersData: any) {
    this.fileUploaded = true;
   this.workersData = workersData;
   this.numOfCUstsInFile = this.workersData.length;

  }


}