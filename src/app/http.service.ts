import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

// const apiUrl = "/api/" //- for Testing purposes;
const apiUrl = "../api/gifts/";
const testApiUrl = "https://test.justforu.co.il/api/Gifts/";
const headers = new HttpHeaders({'Content-Type': 'application/json'});
const token = 'jK9UqnYUsOATCwZUmRjtfse8T2d9zX0sKhoJk5q3Fv41';
const cartValid = 1800000 // 30 Minutes

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  ids: any = {
    OrderId: 0,
    ValidationField: 0
  };

  order = new BehaviorSubject<any>(new Object());
  employee = new BehaviorSubject<any>(new Object());
  employeeOTPCode = new BehaviorSubject<any>(new Object());
  employeeTokenCode = new BehaviorSubject<any>(new Object());
  choice = new BehaviorSubject<any>(null);
  specialDetails: {shopping: string[], delivery: string[], giftSitePointers: any, giftSites: any, deliveryProducts: number[], minQty: number[]} = null;
  shoppingMode = false;
  cart: {key?: {id: number, loadsum: number, name: string, image: string, price: number, qty: number, subtotal: number}}= {};
  GIFTSITE_ITEM_ID = 1443;
  giftSiteItems: any[] = [];
  quantity:any
  linkedTbid = new BehaviorSubject<any>(new Object());
  
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  getOrder(id: string) {
    if (this.route.snapshot.queryParamMap.get('test') == '1')
    this.http.get(apiUrl + 'GetOrderInfo?EncOrder=' + id).subscribe((data: any) => {
      this.order.next(data.response[0]);
    }, () =>  console.log('Error'))
    else
     this.http.get(apiUrl + 'GetOrderInfo?EncOrder=' + id).subscribe((data: any) => {
      this.order.next(data.response[0]);
    }, () =>  console.log('Error'))
  }

  getSpecial() {
    return this.http.get('assets/special.json');
  }

  initCart() {
    if (localStorage.getItem('cart')) {
      let now: any = new Date(), cartUpdate: any = new Date(localStorage.getItem('cartUpdate'));
      if (now - cartUpdate < cartValid) this.cart = JSON.parse(localStorage.getItem('cart'));
    }
  }

  addToCart(item: any, quantity: string) {
    let numQTy = parseInt(quantity);
    if (this.cart[item.id]) {
      let updatedQty: number = parseInt(this.cart[item.id].qty) + numQTy;
      this.cart[item.id].qty = updatedQty;
      this.cart[item.id].subtotal = (parseInt(this.cart[item.id].subtotal) + numQTy * this.cart[item.id].price);
    }
    else {
      this.cart[item.id] = {
        id: item.id,
        loadsum: item.IsGiftCard ? item.LoadSum : 0,
        name: item.name + (item.IsGiftCard ? ('בשווי' + item.LoadSum) : ''),
        image: 'https://www.just4u.co.il/Pictures/' + item.image,
        price: item.Price,
        qty: quantity,
        subtotal: numQTy * item.Price
      }
    }
    localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('cartUpdate', new Date().toString());
  }

  updateQty(itemid: number, quantity: number) {
    this.cart[itemid].qty = quantity;
    this.cart[itemid].subtotal = quantity * this.cart[itemid].price;
    localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('cartUpdate', new Date().toString());
  }

  removeCart(itemid: number) {
    delete(this.cart[itemid]);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    localStorage.setItem('cartUpdate', new Date().toString());
  }

  resetCart() {
    this.cart = {};
    localStorage.removeItem('cart');
    localStorage.removeItem('cartUpdate');
  }

  sendMail(toEmail: string, subject: string, htmlBody: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let params = new HttpParams({
      fromObject: {
        userID: 'TC.justforuAPI',
        password: '2155500',
        toEmail: toEmail,
        toName: 'לקוח דור חדש',
        fromEmail: 'noreply@justforu.co.il',
        fromName: 'דור חדש בשוברים',
        subject: subject,
        HTML: htmlBody,
        languageCode: '0',
        externalRef: ''
      }
    });
    return this.http.post('/ext-api/pulseem/SendEmail', params, {headers: headers, responseType: 'text'});
  }

  sendOTP(employee: JSON) {
    return this.http.post(apiUrl + 'SendOtpObligo', employee, {headers: headers})
    .pipe(catchError(this.handleError));
  }

  validateEmployee(employee: JSON, multipleChoice: boolean) {
    //return this.http.get('assets/response.json');
    this.giftSiteItems = [];
    let reqUrl: string;

    if (multipleChoice) { // ארנק
      if (this.order.value.Type == 2 && this.order.value.isdemo) // אובליגו דמו
        reqUrl = apiUrl + 'ValidateMngObligo';
      else if (this.order.value.Type == 2 && this.employee.value.token) // כניסה ראשונה לאובליגו
        reqUrl = apiUrl + 'ValidateMngObligo';
      else if (this.order.value.Type == 2)
        reqUrl = apiUrl + 'ValidateOtpObligo';
      else
        reqUrl = apiUrl + 'ValidatemultipleChoice';
    }
    //////////////////////////////////////////////////////////////////////////////////
    else // בחירת מתנות
      reqUrl = apiUrl + 'Validate';
      ///////////////////////////////////////////////////////////////////
    return this.http.post(reqUrl, employee, {headers: headers})
    .pipe(map((data: any) => {
      if (data.response && data.response.length) {
        const employee = data.response[0];
        if (employee.ballance || employee.ballance === 0) {
          employee.Obligo = employee.ballance;
          delete employee.ballance;
        }
      }
      return data;
    }),
    catchError(this.handleError));
  }

  doChoice(choice: JSON) {
    return this.http.post(apiUrl + 'DoChoose', choice, {headers: headers})
    .pipe(catchError(this.handleError));
  }

  doChoiceMultiple(choice: JSON) {
    const reqUrl = apiUrl + (this.order.value.Type == 2 ? 'DoChooseMngObligo' : 'DoChooseMultiple');
    return this.http.post(reqUrl, choice, {headers: headers})
    .pipe(catchError(this.handleError));
  }

  sendCode(details: JSON) {
    return this.http.post(apiUrl + 'Push', details, {headers: headers}).pipe(catchError(this.handleError));
  }

  getDelivery(vcVoucher: string) {
    let jsonParams: any = {
      UserId: token,
      VCVoucherid: vcVoucher
    };
    return this.http.post('../api/External/ExternalInfo', jsonParams);
  }

  getSuppliers(itemid: number) {
    return this.http.get('../Api/suppliers/GetSuppliersByItemLite?itemId=' + itemid);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('אירעה שגיאה');
  }

  checkValidation() {
    if (!this.ids.OrderId || !this.ids.ValidationField) {
      //this.routeTo404();
      this.router.navigate([''], { queryParamsHandling: 'preserve' });
      return false;
    }
    return true;
  }

  loginUser(mail: string, password: string) {
    return this.http.post('../api/connections/Login?mail=' + mail + '&password=' + password, {});
  }

  routeTo404() {
    location.href = 'https://www.justforu.co.il/PageNotFound404.aspx';
  }
}
