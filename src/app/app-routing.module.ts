import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectComponent } from './select/select.component';
import { ValidateComponent } from './validate/validate.component';
import { SelectedComponent } from './selected/selected.component';
import { SendToComponent } from './send-to/send-to.component';
import { SendHistoryComponent } from './send-history/send-history.component';
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {path: '', component: ValidateComponent},
  {path: 'select', component: SelectComponent},
  {path: 'selected', component: SelectedComponent},
  {path: 'send-history', component: SendHistoryComponent},
  {path: 'send-to', component: SendToComponent},
  {path: 'cart', component: CartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
