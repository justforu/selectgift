import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpService } from '../http.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

declare function j4UCarousel(wrapper: HTMLElement): void;
const SEND_COOLDOWN: number = 1800000; // 30 Minutes
const MAX_ATTEMPTS: number = 3;

@Component({
  selector: 'app-selected',
  templateUrl: './selected.component.html',
  styleUrls: ['./selected.component.scss']
})
export class SelectedComponent implements OnInit {

  choices: any;
  brands: Array<any> = [];
  vDate: Array<string> = [];
  rDate: Array<string> = [];
  dateOptions: Object = { day: '2-digit', month: '2-digit', year: 'numeric' };
  sendPhone: boolean = false;
  sendMail: boolean = false;
  sendForm: FormGroup;
  sending: boolean = false;
  sent: boolean = false;
  allowSend: boolean = true;
  canChange: boolean = false;
  endDate: string = '';
  suppliers: Array<any> = [];
  showCode = true;

  constructor(private _http: HttpService, private el: ElementRef) { }

  ngOnInit() {
    if (!this._http.checkValidation()) return;
    if (this._http.ids.OrderId == 'y6zOB2fgctxdWLtDxYq_1Q2') this.showCode = false; // pelephone
    if (localStorage.getItem('firstSend')) {
      const firstDate: any = new Date(localStorage.getItem('firstSend')), currDate: any = new Date();
      if (currDate - firstDate < SEND_COOLDOWN) {
        if (parseInt(localStorage.getItem('totalSent')) >= MAX_ATTEMPTS) this.allowSend = false;
      }
    }
    this.choices = this._http.choice.value;
    for (let choice of this.choices) {
      this.brands.push(choice.brands);
      this.vDate.push(new Date(choice.ValidationDate).toLocaleDateString('en-GB', this.dateOptions));
      this.rDate.push((choice.RealizeDate) ? new Date(choice.RealizeDate).toLocaleDateString('en-GB', this.dateOptions) : '');
    }
    let lastDate = new Date(this._http.employee.value.lastdatetochange);
    this.endDate = lastDate.toLocaleDateString('en-GB', this.dateOptions);
    lastDate.setDate(lastDate.getDate() + 1);
    if (this._http.employee.value.canchange && !(new Date() > lastDate)) {
      this.canChange = true;
    }
    this.sendForm = new FormGroup({
      'phone': new FormControl('', Validators.pattern('^05[0-9]{8}$')),
      'email': new FormControl('', Validators.email)
    }, this.checkSend);
  }

  ngAfterViewInit() {
    j4UCarousel(this.el.nativeElement);
    for (let choice of this.choices) {
      this._http.getSuppliers(choice.itemid).subscribe((data: any) => this.suppliers.push(data.dt));
    }
  }

  checkSend(fg: FormGroup) {
    return fg.get('phone').value == '' && fg.get('email').value == '' ? { 'noSend': true } : null;
  }

  onSendSubmit() {
    this.sending = true;
    let sendDetails: any = {
      OrderId: this._http.ids.OrderId,
      ValidationField: this._http.ids.ValidationField
    }
    if (this.sendPhone) {
      let phone = this.sendForm.get('phone');
      if (phone.value && phone.valid) sendDetails.Phone = phone.value;
    }
    if (this.sendMail) {
      let email = this.sendForm.get('email');
      if (email.value && email.valid) sendDetails.Mail = email.value;
    }
    this._http.sendCode(<JSON>sendDetails).subscribe({ complete: () => {
      this.sent = true;
      if (localStorage.getItem('firstSend')) {
        const firstDate: any = new Date(localStorage.getItem('firstSend')), currDate: any = new Date();
        if (currDate - firstDate < SEND_COOLDOWN) {
          let totalSent: number = parseInt(localStorage.getItem('totalSent'));
          ++totalSent;
          localStorage.setItem('totalSent', '' + totalSent);
          if (totalSent >= MAX_ATTEMPTS) this.allowSend = false;
        }
        else this.resetSentStorage();
      }
      else {
        this.resetSentStorage();
      }
      this.sending = false;
    }});
  }

  resetSentStorage() {
    localStorage.setItem('firstSend', new Date().toString());
    localStorage.setItem('totalSent', '1');
  }

  print(itemid: number) {
    this.sendPhone = false;
    this.sendMail = false;
    setTimeout(() => window.print());
  }

}
