import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cart = [];
  orderTotal = 0;
  detailsForm: FormGroup;
  codes = [];
  isError = false;
  attemptError = false;
  isLoading = false;
  completed = false;

  constructor(private http: HttpService, private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    if (!this.http.checkValidation()) return;
    for (let itemid in this.http.cart) {
      this.cart.push(this.http.cart[itemid]);
      this.orderTotal += this.http.cart[itemid].subtotal;
    }
    this.detailsForm = new FormGroup({
      'name': new FormControl(),
      'phone': new FormControl('', Validators.pattern('^0[0-9]{8,9}$')),
      'email': new FormControl('', Validators.email),
      'date': new FormControl(),
      'address': new FormControl(),
      'notes': new FormControl()
    });
  }

  qtyChange(event: any, i: number) {
    // TODO: check minimum, show error
    let newQty = parseInt(event.target.value);
    if (isNaN(newQty) || newQty <= 0) {
      this.isError = true;
      this.renderer.addClass(event.currentTarget.parentElement.parentElement, 'error');
    }
    else {
      if (this.isError) {
        this.renderer.removeClass(event.currentTarget.parentElement.parentElement, 'error');
        setTimeout(() => this.updateErr(), 1);
      }
      this.orderTotal = this.orderTotal - this.cart[i].subtotal + (newQty * this.cart[i].price)
      this.http.updateQty(this.cart[i].id, newQty);
    }
  }

  removeCart(event: any, index: number) {
    this.renderer.addClass(event.currentTarget.parentElement.parentElement, 'removed');
    this.orderTotal = this.orderTotal - this.cart[index].subtotal;
    setTimeout(() => {
      this.http.removeCart(this.cart[index].id);
      this.cart.splice(index, 1);
    }, 300);
  }

  updateErr() {
    if (!this.el.nativeElement.querySelectorAll('.item-block.error').length) this.isError = false;
  }

  submitOrder() {
    this.attemptError = false;
    if (this.detailsForm.invalid || this.orderTotal <= 0 || !this.cart.length || this.isError) {
      this.attemptError = true;
      return;
    }
    this.isLoading = true;
    this.doChooseCart(0, 0);
  }

  doChooseCart(i: number, j: number) {
    let choice: any = {
      OrderId: this.http.ids.OrderId,
      ValidationField: this.http.ids.ValidationField,
      ItemId: this.cart[i].id,
      Name: this.name.value
    }
    if (this.cart[i].loadsum > 0) choice.LoadSum = this.cart[i].loadsum;
    let data: any;
    // TODO: handle errors
    this.http.doChoiceMultiple(<JSON>choice).subscribe({next: (resp) => data = resp, complete: () => {
      this.codes.push({id: this.cart[i].id, name: this.cart[i].name, voucher: data.response[0].VCVoucherid});
      if (j < this.cart[i].qty - 1) this.doChooseCart(i, j + 1);
      else if (i < this.cart.length - 1) this.doChooseCart(i + 1, 0);
      else this.sendMails();
    }});
  }

  sendMails() {
    const tableCart = this.constructCartTable(), tableCodes = this.constructCodesTable();
    let body = encodeURI(`שלום ${this.name.value},
הזמנתך מאושרת.
(הדביקי את הקודים כאן)`);

    let mail = `<style>table, th, td {border: 1px solid black;}</style>שרה שלום,<br><br>
התקבלה הזמנה חדשה של גיבוש מחלקתי דרך דור חדש בשוברים.<br>
להלן פרטי ההזמנה:<br>
שם: ${this.name.value}<br>
טלפון: ${this.phone.value}<br>
מייל: ${this.email.value}<br>
תאריך פעילות: ${this.date.value}<br>
כתובת: ${this.address.value}<br>
הערות: ${this.notes.value}<br>
<br>
שוברים שהוזמנו:<br>
${tableCart}
<br>
סך הכל עלות ההזמנה: ${this.orderTotal} ש"ח.<br>
<br>
<a href="mailto:${this.email.value}?subject=אישור%20הזמנה%20לגיבוש&body=${body}" target="_blank">לחצי לאישור ההזמנה</a><br>
<br>
קודי שובר להעברה למזמין לאחר אישור ההזמנה:<br>
${tableCodes}
<br>
בתודה,<br>
דור חדש בשוברים`;

    let data: any;
    this.http.sendMail('sarap@harel-ins.co.il', 'הזמנה חדשה מ' + this.name.value, mail).subscribe({next: (resp) => data = resp,
      complete: () => {
        console.log('done');
        this.completed = true;
        this.isLoading = false;
        this.cart = [];
        this.http.resetCart();
    }})
  }

  constructCartTable(): string {
    let tableHtml = '<table><tr><th>תיאור</th><th>כמות</th><th>מחיר לעובד</th><th>סך הכל לפריט</th><th>פירוט ומקומות מכבדים</th></tr>';
    for (let item of this.cart) {
      tableHtml += `<tr><td>${item.name}</td><td>${item.qty}</td><td>${item.price}</td><td>${item.subtotal}</td><td><a href="https://www.just4u.co.il/coupon/${item.id}" target="_blank">לינק</a></td></tr>`;
    }
    tableHtml += '</table>';
    return tableHtml;
  }

  constructCodesTable(): string {
    let tableHtml = '<table><tr><th>פריט</th><th>קוד</th></tr>';
    for (let vouch of this.codes) {
      tableHtml += `<tr><td>${vouch.name}</td><td>${vouch.voucher}</td></tr>`;
    }
    tableHtml += '</table>';
    return tableHtml;
  }

  get name() { return this.detailsForm.get('name'); }
  get phone() { return this.detailsForm.get('phone'); }
  get email() { return this.detailsForm.get('email'); }
  get date() { return this.detailsForm.get('date'); }
  get address() { return this.detailsForm.get('address'); }
  get notes() { return this.detailsForm.get('notes'); }
}
