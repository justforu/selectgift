import { Component, Output, EventEmitter } from '@angular/core';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-excel-upload',
  templateUrl: './excel-upload.component.html',
  styleUrls: ['./excel-upload.component.scss']
})
export class ExcelUploadComponent {

  fileLoaded = false;
  numOfCUsts = 0;
  processingResults: any;
  isError = false;
  errorText = '';
  @Output() fileData = new EventEmitter()

  onFileSelected(event: any): void {
    this.fileLoaded = true;
    const file: File = event.target.files[0];
    const fileReader = new FileReader();

    fileReader.onload = (e: any) => {
      const arrayBuffer = e.target.result;
      const workbook = XLSX.read(arrayBuffer, { type: 'array' });

      // Process workbook and extract data from sheets
      this.fileData.emit(this.processWorkbook(workbook));
      console.log (this.processingResults);
    };

    fileReader.readAsArrayBuffer(file);
  }

  processWorkbook(workbook: XLSX.WorkBook): any[] {
    this.numOfCUsts = 0;
    const results: any[] = [];
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];

    const range = XLSX.utils.decode_range(worksheet['!ref']);
    for (let row = range.s.r + 1; row <= range.e.r; row++) {
      this.numOfCUsts += 1;
      const nameCell = worksheet[XLSX.utils.encode_cell({ r: row, c: 0 })];
      const phoneCell = worksheet[XLSX.utils.encode_cell({ r: row, c: 1 })];
      const mailCell = worksheet[XLSX.utils.encode_cell({ r: row, c: 2 })];
      const workerNumCell = worksheet[XLSX.utils.encode_cell({ r: row, c: 3 })];

      if (nameCell &&  (phoneCell || mailCell)) {
        const name = nameCell.v;
        const workerNum = workerNumCell ? workerNumCell.v: '';
        const phone = phoneCell ? phoneCell.v : '';
        const mail = mailCell ? mailCell.v : '';

        // Build the result object and push it to the results array
        results.push({ name, workerNum, phone, mail });
      }
      else {
        this.isError = true;
        this.errorText = 'לא כל השדות מולאו באופן תקין. <br> שדות החובה הם : שם העובד + טלפון או מייל'
      }
    }
    return results;
  }
}
