import { Component, OnInit, Renderer2, ElementRef, HostListener } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';

declare function j4UCarousel(wrapper: HTMLElement): void;

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  employee: any;
  // employee = this._http.employee.value;
  //items = this.employee.items;
  enableCollapsible: boolean = false;
  groups: Array<any> = [];
  selfCoordinatingGroups: Array<any> = [];
  giftSites: any[] = [];
  giftSiteItems: any[];
  // giftSiteItems: any[] = this._http.giftSiteItems;
  priceRanges: number[] = [100, 200, 300, 400];
  order: any;
  //  = this._http.order.value;
  height: number = 0;
  isChange: boolean = false;
  isImmediate: boolean;
  isTBWallet = false;
  poolItmsPssToVlcrd = [];
  // isImmediate: boolean = !(<boolean>this._http.employee.value.canchange || this.order.MultipleChoice == 1);
  modalDialog: boolean = false;
  potentialChoice: any;
  isDemo: boolean;
  isLoading: boolean = false;
  isIE: boolean = false;
  SelfCoordinatingItems: any;
  obligoJustGiftsIds = ['13eOIml9rTpfiCSpjIWJuw2','yDfkx4yAeH6yRworg-ZfrQ2','YJiZLYHB_N7JQJglsqipUw2','KMZQ2OsZdXv5mv5cKp6B6Q2'];
  obligoJustTBIds = ['-WGZIaMIDmhjhjmd7WTyAA2','sMvVZUQAzzct8wQvO5EYpQ2'];
  shoppingMode: any;
  // shoppingMode = this._http.shoppingMode;
  orderTotal = 0;
  cartSize: any;
  // cartSize = Object.keys(this._http.cart).length;
  availbleItems: any[] = [];
  noItemsInBudget = false;
  showGiftsBanner = true;
  showTeamBuildsBanner = true;
  showItems = true;
  constructor(private _http: HttpService, private route: ActivatedRoute, private router: Router, private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    if (!this._http.checkValidation()) return;
    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.userAgent.indexOf('Trident/') !== -1
      || navigator.appVersion.indexOf('Trident/') > -1) {
      this.isIE = true;
      return;
    }
     this.initializeVariables();

    if (this.route.snapshot.queryParamMap.get('change')) this.isChange = true;
    if (this.shoppingMode) {
      for (let itemid in this._http.cart) {
        this.orderTotal += this._http.cart[itemid].subtotal;
      }
    }
    this.employee.items.sort((a,b)=>a.Price - b.Price)
    if (this.order.MultipleChoice == 1 && this._http.specialDetails && this._http.specialDetails.giftSitePointers[this._http.ids.OrderId]) {
      this.giftSites = this._http.specialDetails.giftSites[this._http.specialDetails.giftSitePointers[this._http.ids.OrderId]];
      this.enableCollapsible = true;
      if (this.giftSiteItems.length > 0) {this.employee.items = this.createPriceGroups(this.employee.items); return;}
      for (let site of this.giftSites) { site.items = []; site.Collapsed = true; }
      for (let item of this.employee.items) {
        if (item.id == this._http.GIFTSITE_ITEM_ID) {
          item.hideDefaultView = true;
          for (let i = 0; i < this.giftSites.length; ++i) {
            if (this.giftSites[i].budget == item.LoadSum) {
              this.giftSites[i].giftItemIndex = this.giftSiteItems.length;
              item.giftSiteIndex = i;
              this.giftSiteItems.push(item);
              break;
            }
          }
          continue;
        }
        for (let site of this.giftSites)
          if (site.itemsIds.indexOf(item.id) !== -1) {
            if (!item.IsGiftCard || (site.loadSums && (item.LoadSum == site.loadSums[0] || item.LoadSum == site.loadSums[1]))) {
              item.hideDefaultView = true;
              site.items.push(item);
            }
          }
      }
      
     this.employee.items = this.employee.items.filter(item => !item.hideDefaultViewc);
      if (this.order.MultipleChoice)
      this.employee.items = this.createPriceGroups(this.employee.items);
    }
    else {
      if (this.order.MultipleChoice == 1 && this.order.Type != 2) {
        this.employee.items.forEach(item => { 
          if (item.Price <= this._http.employee.value.Obligo)
            this.availbleItems.push(item);      
        });
        this.groups.push({ Price: null, Collapsed: false, items: this.availbleItems });
        if (this.availbleItems.length == 0) this.noItemsInBudget = true;
      }
      else {
        this.groups.push({ Price: null, Collapsed: false, items: this.employee.items });
      if (this.order.MultipleChoice) {
       if (this.isTBWallet) {
        this.employee.items = this.employee.items.filter(item => !item.hideDefaultViewc&&item.VCPromoId&&!item.GetCodeFromPool );
        this.SelfCoordinatingItems = this.SelfCoordinatingItems.filter(item => !item.hideDefaultViewc&&!item.VCPromoId&&item.GetCodeFromPool);       
        this.SelfCoordinatingItems = this.createPriceGroups(this.SelfCoordinatingItems);
        // this.SelfCoordinatingItems.forEach(item => {
        //   if (item.id == 1192) {
        //     this.employee.items.push(item);
        //     this.SelfCoordinatingItems.splice(this.SelfCoordinatingItems.indexOf(item), 1);
        //   }
        // });
        this.createPriceGroupsForSelfCoordinating(this.SelfCoordinatingItems);
      } 
      this.employee.items = this.createPriceGroups(this.employee.items);
     }
    }
    }
    this.isDemo = this._http.order.value.isdemo;
    if(this.order.Type === 2 && this.order.MultipleChoice) 
    {
      this.showItems =  false;
      // this.showTeamBuildsBanner = this._http.order.value.JustTB;
      // this.showGiftsBanner = this._http.order.value.JustGifts;     
    }
    if(this._http.order.value.JustTB)
    {
      this.showGiftsBanner = false;
      this.navigateToTBPK();
    } 
    if (this._http.order.value.JustGifts) this.showItemsGrid();
    /*if (this.order.MultipleChoice == 1) {
      let price = this.items[0].Price, currItems: Array<any> = [];
      for (let item of this.items) {
        if (item.Price == price) {
          currItems.push(item);
        }
        else {
          this.groups.push({ Price: price, Collapsed: false, Items: currItems });
          price = item.Price;
          currItems = [item];
        }
      }
      this.groups.push({ Price: price, Collapsed: false, Items: currItems });
      if (this.groups.length == 1) this.groups[0].Price = null;
    }
    else {
      this.groups.push({ Price: null, Collapsed: false, Items: this.items });
    }*/
  }

  // Add sub-menus to delivery items

  // canInviteItem(item) {

  //   if (this.order.MultipleChoice == 1 && item.Price > this._http.employee.value.Obligo)
  //    return false;
  //   return true;

  // }

  // setAvailbleItems() {
  //   this.employee.items.forEach(item => {
  //     if (item.Price <= this._http.employee.value.Obligo) {
  //       this.availbleItems = true;
  //       return
  //     }
  //       this.availbleItems = false;
  //   });
  // }

  createPriceGroups1() {
    let i = 0, currItems: Array<any> = [], reachMax = false;
    if (this.isTBWallet){
      this.groups.push({ Price:  'כל השוברים', Collapsed: true, items: this.employee.items});
    }
    for (let item of this.employee.items) {
      if (reachMax || item.Price <= this.priceRanges[i]) {
        currItems.push(item);
      }
      else {
        if (currItems.length)
        if ( i==0 ) this.groups.push({ Price:  this.priceRanges[i] +'20  - ש"ח', Collapsed: true, items: currItems });
         this.groups.push({ Price: this.priceRanges[i - 1] + 'ש"ח - ' + this.priceRanges[i], Collapsed: true, items: currItems });
        while (i < this.priceRanges.length - 1 && item.Price > this.priceRanges[i]) ++i;
        if (item.Price > this.priceRanges[i]) reachMax = true;
        currItems = [item];
      }
    }
    if (currItems.length) {
      if (reachMax) this.groups.push({ Price: 'מעל ' + this.priceRanges[i] + ' ש"ח', Collapsed: true, items: currItems });
      else this.groups.push({ Price: 'עד ' + this.priceRanges[i] + ' ש"ח', Collapsed: true, items: currItems });
    }
    if (this.groups.length == 1) this.groups[0].Price = null;
  }

  createPriceGroups(array: any) {
    let entered = false;
    this.groups = [];
    array.sort((a,b)=>a.Price - b.Price)
    if (this.isTBWallet) this.groups.push({ Price: 'כל השוברים', Collapsed: true, items: array });
    else {
      this.priceRanges.forEach((range, i) => {
        if ( i==0 ) this.groups.push({ Price: ' 20  עד' + this.priceRanges[i] + ' ', Collapsed: true, items: [] });
        else this.groups.push({ Price:this.priceRanges[i-1] + '  עד ' + this.priceRanges[i]+' ', Collapsed: true, items: [] });
      });
      
      this.groups.push({ Price: 'מעל ' + this.priceRanges[this.priceRanges.length-1] + ' ', Collapsed: true, items:[]});
      array.forEach(item=> {
        for(let i = 0;i < this.priceRanges.length;i++){
          if (item.Price < this.priceRanges[i] && !entered){
            this.groups[i].items.push(item);
            entered = true;
          }
        }
        if(!entered)
        this.groups[this.priceRanges.length].items.push(item);
        entered = false;
        });
    }
    
      return array;
    
  }

  createPriceGroupsForSelfCoordinating(array){
    if (this.order.id == 42264) return array; // discount
    let entered = false;
    this.selfCoordinatingGroups = [];
    array.sort((a,b)=>a.Price - b.Price)
    if (this.isTBWallet) this.selfCoordinatingGroups.push({ Price: 'כל השוברים', Collapsed: true, items: array });
    else{
      this.priceRanges.forEach((range, i) => {
        if ( i==0 ) this.selfCoordinatingGroups.push({ Price: ' 20  עד' + this.priceRanges[i] + ' ', Collapsed: true, items: [] });
        else this.selfCoordinatingGroups.push({ Price:this.priceRanges[i-1] + '  עד ' + this.priceRanges[i]+' ', Collapsed: true, items: [] });
      });
      
      this.selfCoordinatingGroups.push({ Price: 'מעל ' + this.priceRanges[this.priceRanges.length-1] + ' ', Collapsed: true, items:[]});
      array.forEach(item=> {
        for(let i = 0;i < this.priceRanges.length;i++){
          if (item.Price < this.priceRanges[i] && !entered){
            this.selfCoordinatingGroups[i].items.push(item);
            entered = true;
          }
        }
        if(!entered)
        this.selfCoordinatingGroups[this.priceRanges.length].items.push(item);
        entered = false;
        });
    } 
    
      return array;
  }

  initializeVariables(){
    this.order = this._http.order.value;
    if ( this.order.id == 42264 || this.order.id == 38605 || this.order.id == 42786 || this.order.id == 42792)
      this.isTBWallet = true;
    if( this.isTBWallet ) {
      this.priceRanges = [10000];
      this.poolItmsPssToVlcrd = [ 1192, 629, 625, 507, 1994, 3031, 3721, 3722, 3723 ]; //pass the items with the real supplier coupons to the top side
      this._http.employee.value.items.forEach(item => {
          if ( this.poolItmsPssToVlcrd.indexOf(item.id) !==-1 ) {
            item.GetCodeFromPool = 0;
            item.VCPromoId = 1;
          }
        });
    }
    this.employee = JSON.parse(JSON.stringify(this._http.employee.value)) ;

    this.SelfCoordinatingItems = this._http.employee.value.items.filter(item => !item.hideDefaultViewc&&!item.VCPromoId&&item.GetCodeFromPool);

    
    this.shoppingMode = this._http.shoppingMode;
    this.giftSiteItems = this._http.giftSiteItems;
    this.isImmediate = !(<boolean>this._http.employee.value.canchange || this.order.MultipleChoice == 1);
    this.cartSize = Object.keys(this._http.cart).length;  
  }
  
  @HostListener('window:resize')
  onResize() {
    this.height = 2 * this.el.nativeElement.querySelector('.gift-item').getBoundingClientRect().height - 56;
    if (this.enableCollapsible) {
      let openGrids: Array<HTMLElement> = Array.from(this.el.nativeElement.querySelectorAll('.collapsible-grid'));
      openGrids = openGrids.filter((ele) => ele.style.maxHeight != '0px');
      setTimeout(() => {
        for (let grid of openGrids) {
          grid.style.maxHeight = grid.scrollHeight + 'px';
        }
      });
    }
  }

  ngAfterViewInit() {
    this.height = 2 * this.el.nativeElement.querySelector('.gift-item').getBoundingClientRect().height - 56;
    console.log(this.employee);
    /*if (this.enableCollapsible) {
      let openGrids: Array<HTMLElement> = this.el.nativeElement.querySelectorAll('.gifts-grid');
      for (let grid of openGrids) {
        grid.style.maxHeight = grid.scrollHeight + 'px';
      }
    }*/
  }

  expand(event: any) {
    //let wrapper = event.currentTarget.closest('.gift-wrapper');
    let wrapper = event.currentTarget.parentElement;
    let grid = event.currentTarget.parentElement.parentElement;
    //let grid = event.currentTarget.closest('.gifts-grid');
    this.renderer.addClass(wrapper, 'expanded');
    setTimeout(() => {
      j4UCarousel(wrapper);
      if (this.enableCollapsible) grid.style.maxHeight = grid.scrollHeight + 'px';
    });
  }

  collapse(event: any) {
    event.stopPropagation();
    this.renderer.removeClass(event.currentTarget.parentElement.parentElement, 'expanded');
    let itemfooter = event.currentTarget.parentElement.parentElement.querySelector('.brief-footer');
    this.renderer.removeClass(itemfooter, 'addtocart');
    this.renderer.removeClass(itemfooter, 'added-ok');
  }

  openQty(event: any) {
    this.renderer.addClass(event.currentTarget.parentElement, 'addtocart');
  }

  addToCart(event: any, item: any, quantity: string) {
    if (!this._http.cart[item.id]) this.cartSize++;
    this._http.addToCart(item, quantity);
    this.orderTotal += parseInt(quantity) * item.Price;
    this.renderer.addClass(event.currentTarget.parentElement, 'added-ok');
  }

  doPotentialChoice(item: any) {
    this.isImmediate = !(<boolean>this._http.employee.value.canchange || this.order.MultipleChoice == 1) || this.isDeliveryItem(item.id)
    if (this.isImmediate && this.employee.items.length > 1) {
      this.potentialChoice = item;
      this.modalDialog = true;
    }
    else this.doChoice(item);
  }

  doChoice(item: any) {
    this.isLoading = true;
    let vcvoucher = '';
    if (this.order.MultipleChoice == 1 && !this.isDeliveryItem(item.id)) {
      this._http.choice.next(item);
      this.router.navigate(['/send-to'], { queryParamsHandling: 'preserve' });
      return;
    }
    let choice: any = {
      OrderId: this._http.ids.OrderId,
      ValidationField: this._http.ids.ValidationField,
      ItemId: item.id
    };
    let employee: any;
    if (this._http.order.value.Type === 2)
      employee = {
        ValidationField: this._http.ids.ValidationField,
        OrderId: this._http.ids.OrderId,
        PackageId: this._http.linkedTbid,
        Token: this._http.employee.value.token,
      }
      else employee = <JSON>this._http.ids;
    if (item.IsGiftCard) choice.LoadSum = item.LoadSum;
    if (this.isDeliveryItem(item.id)) {
      choice.Name = '';
      choice.FromWho = '';
      choice.MailMsg = '';
      this._http.doChoiceMultiple(<JSON>choice).subscribe({
        next: (resp: any) => { if (resp && resp.response) vcvoucher = resp.response[0].VCVoucherid; },
        complete: () => {
          this._http.validateEmployee(employee, this._http.order.value.MultipleChoice == 1).subscribe({
            next: (data: any) => { if (data.response){
              employee = data.response[0];
              employee.token = data.Token;
              let tempItems = this._http.employee.value.items;
              this._http.employee.next(employee) 
              this._http.employee.value.items = tempItems;
              this._http.employee.next(employee)
            }  }, complete: () => {
              this._http.employee.value.history.reverse();
              window.open('https://www.just4u.co.il/LandDelivery/?voucher=' + vcvoucher);
              setTimeout(() => {
                this.router.navigate(['/send-history'], { queryParamsHandling: 'preserve' });
              }, 10);
            }
          });
        }
      });
    }
    else this._http.doChoice(<JSON>choice).subscribe({
      complete: () => {
        this._http.validateEmployee(employee, this.order.MultipleChoice == 1).subscribe({
          next: (data: any) => this._http.choice.next(data.response[0].history), complete: () => {
            this.router.navigate(['/selected'], { queryParamsHandling: 'preserve' });
          },
          error(err) {
            this.isError = true;
            if (err.err == -31)
            this.errorText = 'אחד מהתווים שהזנת באחד מהשדות לא חוקיים. אנא מחק/י תוים מיוחדים'
          },
        });
      }
    });
  }

  isDeliveryItem(itemid: number): boolean {
    return this._http.specialDetails.delivery.indexOf(this._http.ids.OrderId) !== -1 && this._http.specialDetails.deliveryProducts.indexOf(itemid) !== -1
  }

  cancelChange() {
    this.router.navigate(['/selected'], { queryParams: { id: this._http.ids.OrderId } });
  }

  navigateToTBPK() {
    // if (this._http.ids.OrderId == 'jwfrK-60-i7nDAyTo1DAWw2')
    //   return;
    // if (this._http.ids.OrderId == 'hgRoRt29r-EZuFm-Dzj5Tw2')
    this.isLoading = true;
    if (this.route.snapshot.queryParamMap.get('test') == '1')
      location.href = 'https://www.just4u.co.il/SelectActivity?id='+this._http.linkedTbid.value+'&userid=' + this._http.ids.ValidationField + '/' + this.employee.token + '&linkedorder=' + this._http.ids.OrderId + '&test=1';
    else  location.href = 'https://www.just4u.co.il/SelectActivity?id='+this._http.linkedTbid.value+'&userid=' + this._http.ids.ValidationField + '/' + this.employee.token + '&linkedorder=' + this._http.ids.OrderId;
    // location.href = 'https://www.just4u.co.il/SelectActivity?id=nd9aYDLdnH6L__A1f-V0nw2&userid=' + this._http.ids.ValidationField + '/' + this._http.employeeOTPCode.value + '&linkedorder=' + this._http.ids.OrderId;
  }
  showItemsGrid() {
    this.showItems = true;
  }

}
