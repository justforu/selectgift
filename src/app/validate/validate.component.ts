import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.scss']
})
export class ValidateComponent implements OnInit {

  id: string;
  order: any;
  vldField: number = 0;
  vldLabel: string = '';
  vldValue: string = '';
  vldPass: string = '';
  both: boolean = false;
  isError: boolean = false;
  isLoading = false;
  errorText: string;
  OTPSent = false;
  OTPValue = '';
  OTPState = false;
  comeFromTB = false;
  isDemo = false;
  
  constructor(private _http: HttpService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.isDemo = this.route.snapshot.queryParamMap.get('test') == '1';
    if (this._http.employee.value.token){
      this.isLoading = true;
      this.comeFromTB = true;
      this.routeTo('/select');
    }
    if (this.route.snapshot.queryParamMap.get('userid')){
      // this.ri
        this.isLoading = true;
        this.comeFromTB = true;
        let employee = {token:''};
        let userDatails = this.route.snapshot.queryParamMap.get('userid').split("/");
        this._http.ids.ValidationField = userDatails[0]; 
        employee.token = userDatails[1]
        this._http.employee.next(employee);

        this._http.linkedTbid.next(this.route.snapshot.queryParamMap.get('linkedtbid'));
        if (this.route.snapshot.queryParamMap.get('orderid')){
          this.id = this.route.snapshot.queryParamMap.get('orderid');
          this._http.ids.OrderId = this.id;
        }
        this.OTPSent =true;
        this.OTPState = true;
        debugger
      }

   else { 
    if (this.route.snapshot.queryParamMap.get('id'))
     this.id = this.route.snapshot.queryParamMap.get('id');
    if (this.route.snapshot.queryParamMap.get('orderid'))
      this.id = this.route.snapshot.queryParamMap.get('orderid');
    if (this.route.snapshot.queryParamMap.get('tbid')) {
      this._http.linkedTbid.next(this.route.snapshot.queryParamMap.get('tbid'));}
    }
      
    
    if (this.id) { 
      if (this.id == '1hyUy7jAROGfC-Ml-jC6lA2') {
        location.href = 'https://test.justforu.co.il/SelectGiftNew/?id=bny9JU8aWpIetI1ahV6SOg2';
      }
      else {
        debugger
        this._http.getOrder(this.id);
        if (!this._http.order.value.Type)
        setTimeout(() => {}, 3000);
        this._http.order.pipe(take(2)).subscribe({
          next: data => this.order = data, complete: () => {
            if (this.order.isdemo) {
              this._http.ids.OrderId = this.id;
              this.vldValue = '123456';
              this.onVldSubmit();
              return;
            }
          if (this.comeFromTB) { //come from tbpkcgs
            this.onVldSubmit();
            return;
          }
        this.OTPState = (this.order.Type == 2 && this.order.MultipleChoice);
            this.vldField = this.order.validatortype;
            if (this.order.MultipleChoice == 1 || this.id == 'kd-Z-MhUeEkg75xkGxAxbg2') this.vldLabel = 'קוד מזהה'; 
            if (this.id == 'KMZQ2OsZdXv5mv5cKp6B6Q2') this.vldLabel = 'מספר טלפון ללא מקף';
            
            else switch (this.vldField) {
              case 1: {
                this.vldLabel = 'מספר תעודת זהות';
                break;
              }
              case 2: {
                this.vldLabel = 'מספר עובד';
                break;
              }
              case 5: {
                this.vldLabel = 'תאריך לידה (6 ספרות) + מספר סניף/מחלקה (3 ספרות) ברצף. למשל, עבור התאריך 01.01.2001 וסניף 999, יש להזין 010101999';
                break;
              }
              case 3: {
                this.vldLabel = 'מספר טלפון';
                break;
              }
              case 4: {
                this.vldLabel = 'כתובת מייל';
              }
            }
            this._http.ids.OrderId = this.id;
          }
        });
        
      }
    }
        this.updateSpecial();
  }

  sendOTP(employee: any) {
    let data: any;
    this._http.sendOTP(<JSON>employee).subscribe({next: (resp) => {data = resp}, 
    error: () =>{this.isLoading = false; this.isError = true; },
       complete: () => {
        this.isLoading = false
        if (data.err){
          this.isError = true;
          this.errorText = 'הוקשה סיסמא שגויה / משתמש לא קיים במערכת';
          if (data.err === -41){
            this.errorText = 'לצערינו, אין הרשאת גישה לכתובת ממנה אתם פונים לאתר.'
          } 
        } else {
          this.errorText = '';
            this.OTPSent = true
            this.vldLabel = 'את הקוד שקיבלתם';
            return;
          }
          
          
        } });
       
  }

  updateSpecial() {
    this._http.getSpecial().subscribe({next: (resp: any) => this._http.specialDetails = resp, complete: () => {
      if (this._http.specialDetails.shopping.join().indexOf(this.id) !== -1) this._http.shoppingMode = true;
      this._http.initCart();
    }});
  }

  onVldSubmit() {
    this.isLoading = true;
    let employee: any;
    const multiple = this.order.MultipleChoice == 1;
    if (this.order.Type == 2 && this.isDemo) { // אובליגו דמו
      employee = {
        ValidationField: '123456',
        OrderId: this._http.ids.OrderId,
        PackageId: this._http.linkedTbid.value        
      }

    }
     if (this.OTPState&&this.OTPSent) // אובליגו כניסה שניה
      if (this.comeFromTB)
      employee = {
        ValidationField: this._http.ids.ValidationField,
        OrderId: this._http.ids.OrderId,
        PackageId: this._http.linkedTbid.value,
        Token: this._http.employee.value.token,
      }
      else 
      employee = {
       ValidationField: this.vldValue.trim(),
       OrderId: this.id,
        PackageId: this._http.linkedTbid.value,
       OTP: this.OTPValue
    };
    else if(this.order.Type != 2) employee = { //לא אובליגו
        ValidationField: this.vldValue.trim(),
        OrderId: this.id,
     }; 
    if(this.OTPState&&!this.OTPSent&&!this.order.isdemo){//אובליגו כניסה ראשונה
      employee = {
        OrderId: this.id,
        ValidationField: this.vldValue.trim(),
        PackageId: this._http.linkedTbid.value
      }
      this.sendOTP(employee);
     }
    else {
      this._http.validateEmployee(<JSON>employee, multiple).subscribe({
        next: (data: any) => {
          if (data.err)
          if (data.err === 303){
            this.isLoading = false;
            this.errorText = 'הקוד שגוי, נא לנסות שנית';
            this.isError = true;
            return;
          }
          employee = data.response;
          employee.token = data.Token;
          this._http.employee.next(employee);
          
        }, error: () => {this.isLoading = false; this.isError = true;}, 
        complete: () => 
        {
          this.isLoading = false;
          if (this._http.employee.value == null && !this.isError || !this._http.employee.value.length && !this.isError) {
            this.notIdentified();
            return;
          }

          
          
          if (this.order.Type == 2){
            employee = this._http.employee.value[0];
            if (!this.isDemo)
              employee.token = this._http.employee.value.token;
            this._http.employee.next(employee);
            if (this.comeFromTB) {
              if (employee.Obligo == 0) {
                this.routeTo('/send-history');
                return;
              }
              else {
                this.routeTo('/select');
                return; 
              }
            }
          }
          else {
            this._http.employee.next(this._http.employee.value[0]);
          }
          if (multiple && this._http.employee.value.history) this._http.employee.value.history.reverse();
          employee = this._http.employee.value;
          if (employee.history == null && employee.items == null) {
            this.notIdentified();
            return;
          }
          if (multiple && this.vldField == 4) {
            let token = null;
            this._http.loginUser(this.vldValue.trim(), this.vldPass).subscribe({
              next: (data: any) => token = data.Token,
              error: () => {
                this.notIdentified();
                return;
              },
              complete: () => {
                if (!token) {
                  this.notIdentified();
                  return;
                } else this.validationHelper(employee, true);
              }
            });
          } else this.validationHelper(employee, multiple);
          this.validationHelper(employee, multiple);

        }
      });
     }
   
  }

  validationHelper(employee: any, multiple: boolean) {
    this._http.employee.next(employee)
    if (this.order.Type == 2 && this.order.MultipleChoice && !this.isDemo)
     this._http.employee.value.otp = this.OTPValue;
    if (!this.comeFromTB)
      this._http.ids.ValidationField = this.vldValue.trim();
    if (employee.history == null || multiple) {
      let expiryDate = multiple ? new Date(this.order.vieworderenddate) : new Date(this.order.vieworderenddate);
      expiryDate.setDate(expiryDate.getDate() + 1);
      let today = new Date();
      if (today > expiryDate) {
        this.errorText = 'מצטערים, לא ניתן לבחור עוד מתנות באתר זה :(';
        this.isError = true;
        this.isLoading = false;
        return;
      }
      if (this.order.isdemo && this._http.employee.value.items) {
        this.routeTo('/select');
        return;
      }
      if (employee.Obligo == 0 && multiple) {
        this.routeTo('/send-history');
        return;
      }
      if (employee.orderid || employee.OriginOrdrid)
        this.routeTo('/select');
    }
    else {
      this._http.choice.next(this._http.employee.value.history);
      if (employee.items == null || employee.canchange) this.routeTo('/selected');
      else this.both = true;
    }
  }

  checkObligo(employee){
    if (!employee) return false;
    if (employee.Obligo == 0) return false;
    if (!employee.items) return false;
    let obligo = false;
    employee.items.forEach(item => {
      if (employee.Obligo > item.Price)
        obligo = true
    });
   return obligo;
  }

  notIdentified() {
    this.isLoading = false;
    this.errorText = 'מצטערים, לא מצאנו אתכם...\nאנא פנו לארגון שלכם או לשירות דור חדש בשוברים בטלפון: 03-5322313';
    this.isError = true;
  }

  routeTo(path: string) {
    this.router.navigate([path], { queryParamsHandling: 'preserve' });
  }

}