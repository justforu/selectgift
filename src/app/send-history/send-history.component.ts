import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as XLSX from 'xlsx'; 
import { SchedualedCusts } from '../interfaces';

const SEND_COOLDOWN: number = 1800000; // 30 Minutes
const MAX_ATTEMPTS: number = 3;

@Component({
  selector: 'app-send-history',
  templateUrl: './send-history.component.html',
  styleUrls: ['./send-history.component.scss']
})
export class SendHistoryComponent implements OnInit {

  constructor(private _http: HttpService) { }

  employee: any;
  noObligo: boolean;
  isMngObligoOrder: boolean;
  excel: boolean = false;
  sending: boolean = false;
  sent: boolean = false;
  allowSend: boolean = true;
  selectedOrder: any;
  showModal = false;
  custsCols: any;
  custs: SchedualedCusts[];
  isLoading = false;
  orderId: string;

  ngOnInit() {
    this.employee = this._http.employee.value;
    this.isMngObligoOrder = this._http.order.value.Type == 2;
    this.noObligo = !this.checkObligo(); 
    this.orderId = this._http.ids.OrderId;
    if (!this._http.checkValidation()) return;
    if (!this.employee.history || !this.employee.history.length) return;
    if (!this.isMngObligoOrder) {
      const itemPriceMap = Object.assign({}, ...this.employee.items.map((item: any) => ({[item.id]: item.Price})));
      const giftCardPriceMap = Object.assign({}, ...this.employee.items.filter((item: any) => item.IsGiftCard).map((item: any) => ({[item.id + '-' + item.LoadSum]: item.Price})));
      const dateOptions: Intl.DateTimeFormatOptions = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
      };
      for (let item of this.employee.history) {
        if (!item.IsGiftCard) item.Price = itemPriceMap[item.itemid];
        else item.Price = giftCardPriceMap[item.id + '-' + item.LoadSum];
        if (item.Email && item.Phone) item.mailPhone = item.Email + ', ' + item.Phone;
        else item.mailPhone = item.Email ? item.Email : item.Phone;
        let dateString = item.Sent.split(' '), day = dateString[0], hour = dateString[1].substring(0, 5);
        day = day.split('-').reverse().join('/');
        item.sendDate = day + ', ' + hour;
        item.vDate = new Date(item.ValidationDate).toLocaleDateString('en-GB', dateOptions);
        if (item.VCVoucherid == '62001981164') item.dnum = 2318862;
      }
    }
    else this.employee.history = this.employee.history.filter((currentItem) => currentItem.source !== "TB");
    this.employee.history.forEach(cust => {
      this.custs.push({
        orderid: cust.orderid,
        itemname: cust.itemname,
        PriceWithVAT: cust.PriceWithVAT,
        LoadSum: cust.Loadsum,
        ToName: cust.ToName,
        ToPhone: cust.ToPhone,
        toEmail: cust.toEmail,
        MDate: cust.Mdate,
        MailMsg: cust.MailMsg,
        isEditing: false
      });
    });
    this.employee.history.sort((a, b) => b.orderid - a.orderid);
    this.employee
    this.custsCols = [
      { field: 'orderid', header: 'מספר הזמנה' },
      { field: 'itemname', header: 'שם פריט' },
      { field: 'PriceWithVAT', header: 'סכום' },
      { field: 'LoadSum', header: 'סכום טעינה'},
      { field: 'ToName', header: 'נשלח אל' },
      { field: 'ToPhone', header: 'טלפון' },
      { field: 'toEmail', header: 'מייל' },
      { field: 'MDate', header: 'תאריך שליחה' },
      { field: 'MailMsg', header: 'ברכה' },
      { field: '', header: 'פעולות' },
    ];
  }

  checkObligo(){
    if (!this.employee) return false;
    if (this.employee.Obligo == 0) return false;
    if (!this.employee.items) return false;
    let obligo = false;
    this.employee.items.forEach(item => {
      if (this.employee.Obligo >= item.Price)
        obligo = true
    });
   return obligo;
  }

  exportExcel() {
    this.excel = true;
    setTimeout(() => {
      let tbl = document.getElementById('excel-table'); 
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(tbl);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
      wb.Workbook = {Views: [{}]};
      wb.Workbook.Views[0].RTL = true;
      XLSX.writeFile(wb, 'GiftsSent_' + new Date().toLocaleDateString() + '.xlsx');
    }, 1);
  }

  resend(order:any){
    this.sending = true;
    this.selectedOrder = order;
    this.showModal = true;
    this._http.sendCode(<JSON>this.selectedOrder).subscribe({ complete: () => {
      this.sent = true;
      if (localStorage.getItem('firstSend')) {
        const firstDate: any = new Date(localStorage.getItem('firstSend')), currDate: any = new Date();
        if (currDate - firstDate < SEND_COOLDOWN) {
          let totalSent: number = parseInt(localStorage.getItem('totalSent'));
          ++totalSent;
          localStorage.setItem('totalSent', '' + totalSent);
          if (totalSent >= MAX_ATTEMPTS) this.allowSend = false;
        }
        else this.resetSentStorage();
      }
      else {
        this.resetSentStorage();
      }
      this.sending = false;
    }});
  }

  resetSentStorage() {
    localStorage.setItem('firstSend', new Date().toString());
    localStorage.setItem('totalSent', '1');
  }

  saveManager(cust: SchedualedCusts) {

  }

  cancelEdit(cust: SchedualedCusts) {

  }

}
