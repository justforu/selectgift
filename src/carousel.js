function j4UCarousel(wrapper) {
    const slider = wrapper.querySelector('.slider');
    const next = document.querySelector('.next');
    const prev = document.querySelector('.prev');
    let direction = 1;

    if (slider != null) {
        next.addEventListener('click', function () {
            if (direction == -1) {
                direction = 1;
            }
            slider.style.transition = '';
            slider.style.transform = 'translateX(20%)';
        });

        prev.addEventListener('click', function () {
            if (direction == 1) {
                direction = -1;
            }
            slider.prepend(slider.lastElementChild);
            slider.style.transform = 'translateX(20%)';
            setTimeout(function () { slider.style.transition = ''; slider.style.transform = 'translateX(0)'; });
        });

        slider.addEventListener('transitionend', function () {
            slider.style.transition = 'none';
            if (direction == 1) {
                slider.appendChild(slider.firstElementChild);
                slider.style.transform = 'translateX(0)';
            }
        }, false);
    }
}